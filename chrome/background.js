const LOADING_STATE = 'loading';
const LISTED_STATE = 'listed';
const UNLISTED_STATE = 'unlisted';
const DISABLED_STATE = 'disabled';
const PUBLIC_STATE = 'public';

let currentRequest = null;

const isUrlValid = (url) => url && url.match(/^http/);

const setTabIcon = (tabId, status) => {
  chrome.browserAction.setIcon({
    tabId,
    path: {
      '16': `assets/images/${status}_16.png`,
      '48': `assets/images/${status}_48.png`,
      '128': `assets/images/${status}_128.png`
    }
  });
};

const checkUrl = (tabId, url) => {
  if (!url) {
    return
  }

  if (!isUrlValid(url)) {
    setTabIcon(tabId, DISABLED_STATE);
    return
  }

  if (currentRequest) {
    currentRequest.abort();
  }

  chrome.storage.sync.get('apiKey', ({ apiKey }) => {
    const info = parseApiKey(apiKey);
    if (!info) {
      setTabIcon(tabId, PUBLIC_STATE);
      currentRequest = $.get('https://app.semaforo.tech/v1/check-url',{ url })
        .done(data => setTabIcon(tabId, data && data.listed ? LISTED_STATE : UNLISTED_STATE))
        .fail(() => setTabIcon(tabId, UNLISTED_STATE));
    } else {
      // TODO: don't send the full URL (only domain and subdomain)
      const { checker, token } = info;
      setTabIcon(tabId, LOADING_STATE);
      currentRequest = $.get(checker, { token, url })
        .done(data => setTabIcon(tabId, data && data.listed ? LISTED_STATE : UNLISTED_STATE))
        .fail(() => setTabIcon(tabId, UNLISTED_STATE));
    }
  });
};

chrome.tabs.onUpdated.addListener((tabId, { url }) => {
  checkUrl(tabId, url);
});

chrome.tabs.onActivated.addListener(({ tabId }) => {
  chrome.tabs.get(tabId, ({ url }) => checkUrl(tabId, url));
});
