const parseApiKey = (apiKey) => {
  let data = null;
  try {
    data = jwt_decode(apiKey);
  } catch (err) {
    return null;
  }

  const { checker, token } = data;
  if (!checker || !token) {
    return null;
  }

  return data;
};
