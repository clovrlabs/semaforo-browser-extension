$(() => {
  $('#title').text(chrome.i18n.getMessage('options_title'))
  $('#instructions').text(chrome.i18n.getMessage('options_instructions'))
  $('#apiKey_label').text(chrome.i18n.getMessage('options_apiKey'))

  const apiKeyField = $('#apiKey')
  const saveBtn = $('#save')
  const message = $('#message')

  const showErrorMsg = (msg) => {
    message.attr('class', 'error').text(msg)
  }

  const showSuccessMsg = (msg) => {
    message.attr('class', 'success').text(msg)
  }

  apiKeyField.on('input', () => {
    message.text('')
  })

  saveBtn.on('click', () => {
    const apiKey = apiKeyField.val().trim()

    let info = parseApiKey(apiKey)
    if (!info) {
      showErrorMsg(chrome.i18n.getMessage('options_invalidApiKey'))
      return
    }

    chrome.storage.sync.set({ apiKey }, () => {
      showSuccessMsg(chrome.i18n.getMessage('options_success'))
    })
  })
})